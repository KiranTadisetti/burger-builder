import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux';

import Aux from '../../hoc/Auxilary/Auxilary'
import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from "../../components/Burger/OrderSummary/OrderSummary"
import axios from '../../axios-orders'
import Spinner from '../../components/UI/Spinner/Spinner';
import WithErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../store/actions/index';


const burgerBuilder = props => {


    const [purchasing, setPurchasing] = useState(false);

    useEffect(() => {
        props.onInitIngredients();
    }, [props.onInitIngredients]);

    const updatePurchaseState=(ingredients) =>{

        const sum = Object.keys(ingredients)
            .map(igKey => {
                return ingredients[igKey];
            })
            .reduce((sum, el) => {
                return sum + el
            }, 0);

        return sum > 0;
    }


    const purchaseHandler = () => {
        if (props.isAuthenticated) {
            setPurchasing( true );
        }
        else {
            props.onSeteAuthRedirectPath('/checkout');
            props.history.push("/auth");
        }
    }
    const purchaseCancelHandler = () => {
        setPurchasing( false );
    }

    const purchaseContinueHandler = () => {
        props.onInitPurchase();
        props.history.push("/checkout");
    }


        const disabledInfo = {
            ...props.ings
        };

        for (let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }
        let orderSummanry;

        // if (this.state.loading) {
        //     orderSummanry = <Spinner />;
        // }
        let burger = props.error ? <p>Ingredients cannot be loaded</p> : <Spinner />
        if (props.ings !== null) {
            burger = (
                <Aux>
                    <Burger ingredients={props.ings} />
                    <BuildControls
                        added={props.onIngredientAdded}
                        removed={props.onIngredientRemoved}
                        disabled={disabledInfo}
                        price={props.price}
                        purchasable={updatePurchaseState(props.ings)}
                        checkout={purchaseHandler}
                        isAuth={props.isAuthenticated}
                    />
                </Aux>
            )
            orderSummanry = <OrderSummary ingredients={props.ings}
                purchaseCancelled={purchaseCancelHandler}
                purchaseContinue={purchaseContinueHandler}
                price={props.price}
            />
        }
        return (
            <Aux>
                <Modal show={purchasing} modalClosed={purchaseCancelHandler} >
                    {orderSummanry}
                </Modal>
                {burger}
            </Aux>
        )
   


}

const mapStateToProps = (state) => {
    return {
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        error: state.burgerBuilder.error,
        isAuthenticated: state.auth.token
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onIngredientAdded: (ingName) => dispatch(actions.addIngredient({ ingredientName: ingName })),
        onIngredientRemoved: (ingName) => dispatch(actions.removeIngredient({ ingredientName: ingName })),
        onInitIngredients: () => dispatch(actions.initIngredients()),
        onInitPurchase: () => dispatch(actions.purchaseInit()),
        onSeteAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))

    }
}


export default connect(mapStateToProps, mapDispatchToProps)(WithErrorHandler(burgerBuilder, axios));
import React, { useState } from 'react';
import { connect } from 'react-redux';

import axios from '../../../axios-orders';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler'

import Button from '../../../components/UI/Button/Button';
import classes from './ContactData.css';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Input from '../../../components/UI/Input/Input';
import * as orderActions from '../../../store/actions/index';
import { updateObject, checkValidation } from '../../../shared/utility';

const contactData = props => {

    const [orderForm, setOrderForm] = useState({
        name: {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'Your Name'
            },
            value: '',
            validation: {
                required: true,
                minLength: 3
            },
            valid: false,
            touched: false
        },
        street: {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'Your Street'
            },
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        },
        ZipCode: {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'Your ZIP Code'
            },
            value: '',
            validation: {
                required: true,
                minLength: 6,
                maxLength: 6,
                isNumeric: true
            },
            valid: false,
            touched: false
        },
        email: {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'Your E-mail'
            },
            value: '',
            validation: {
                required: true,
                isEmail: true
            },
            valid: false,
            touched: false
        },
        deliveryMethod: {
            elementType: 'select',
            elementConfig: {
                options: [{ value: 'fastest', displayValue: 'Fastest' },
                { value: 'cheapest', displayValue: 'Cheapest' }]
            },
            value: 'fastest',
            validation: {
                required: false
            },
            valid: true,
            touched: false
        }
    });

    const [formIsValid, setFormIsValid] = useState(false);


    const orderHandler = (event) => {
        event.preventDefault();
        //console.log(this.props.ingredients);
        const formData = {};
        for (let formElementIndentifier in orderForm) {
            formData[formElementIndentifier] = orderForm[formElementIndentifier].value;
        }
        const order = {
            ingredients: props.ings,
            price: props.price,
            orderData: formData,
            userId: props.userId
        }
        props.onOrderBurger(props.token, order);

    }


    const inputChangedHandler = (event, inputIdentifier) => {
        //console.log(event.target.value,inputIdentifier);

        const updatedFormElement = updateObject(orderForm[inputIdentifier], {
            value: event.target.value,
            touched: true,
            valid: checkValidation(event.target.value, orderForm[inputIdentifier].validation)
        });


        const updatedOrderForm = updateObject(orderForm, {
            [inputIdentifier]: updatedFormElement
        });

        let formIsValid = true;
        for (let inputIdentifier in updatedOrderForm) {
            formIsValid = formIsValid && updatedOrderForm[inputIdentifier].valid;
        }

        setOrderForm(updatedOrderForm)
        setFormIsValid(formIsValid);

        //console.log(updatedFormElement.value + ":" + updatedFormElement.valid)

    }

    const formElementArray = Object.keys(orderForm).map(elementKey => {
        return { id: elementKey, config: orderForm[elementKey] }
    })
    let form = (
        <form onSubmit={orderHandler}>
            {formElementArray.map(element => {
                return <Input key={element.id}
                    valueType={element.id}
                    elementType={element.config.elementType}
                    elementConfig={element.config.elementConfig}
                    value={element.config.value}
                    invalid={!element.config.valid}
                    shouldValidate={element.config.validation}
                    touched={element.config.touched}
                    changed={(event) => inputChangedHandler(event, element.id)}
                />
            })}
            <Button btnType='Success' disabled={!formIsValid}>ORDER</Button>
        </form>
    );
    form = props.loading ? <Spinner /> : form;
    return (
        <div className={classes.ContactData}>
            <h4>Enter your Contact Data</h4>
            {form}
        </div>
    )


}


const mapStateToProps = (state) => {
    return {
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        loading: state.order.loading,
        token: state.auth.token,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onOrderBurger: (token, orderData) => dispatch(orderActions.purchaseBurger(token, orderData))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(contactData, axios));
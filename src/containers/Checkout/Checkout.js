import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import ContactData from './ContactData/ContactData';
import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';

const checkout = props=> {

    // componentWillMount(){ uses last props
    //     this.props.onPurchaseInit();
    // }

    const checkoutCancelledHandler = () => {
        props.history.goBack();
    }

    const checkoutContinuedHandler = () => {
        props.history.replace("/checkout/contact-data");
    }
    
        let summary = <Redirect to='' />
        if (props.ings) {
            const purchasedRedirect = props.purchased?<Redirect to='/'/>:null;
            summary = <div><CheckoutSummary
                ingredients={props.ings}
                onCheckoutContinued={checkoutContinuedHandler}
                onCheckoutCancelled={checkoutCancelledHandler}
            />
            {purchasedRedirect}
                <Route path={props.match.path + '/contact-data'} component={ContactData} />
            </div>
        }


        return summary
   
}

const mapStateToProps = (state) => {
    return {
        ings: state.burgerBuilder.ingredients,
        purchased:state.order.purchased
    }
}


export default connect(mapStateToProps)(checkout);
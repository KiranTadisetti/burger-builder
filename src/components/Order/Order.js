import React from 'react';

import classes from './Order.css';

const order = (props) => {
    const ingredientOutput=Object.keys(props.ingredients).map(ig =>{
        return <span key={ig} style={{
            textTransform: 'capitalize',
            display: 'inline-block',
            margin: '0 8px',
            border: '1px solid #ccc',
            padding: '5px'
        }}>{ig}:{props.ingredients[ig]}</span>
    });

    // const ingredientOutput =ingredients.map(ig=>{
    //     return <
    // })
    //console.log(props);
    return (
        <div className={classes.Order}>
            <p>Ingredients:{ingredientOutput} </p>
            <p>Price: <strong>USD {Number(props.price).toFixed(2)}</strong></p>
        </div>
    )

}

export default order;
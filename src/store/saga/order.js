import { put } from 'redux-saga/effects';

import axios from '../../axios-orders'

import * as actions from '../actions/';

export function* purchaseBurgerSaga(action) {
    yield put(actions.purchaseBurgerStart());
    //only for firebase the endpoint will be .json
    try {
        const response = yield axios.post("/orders.json?auth=" + action.token, action.orderData)
        yield put(actions.purchaseBurgerSuccess(response.data.name, action.orderData));

    }
    catch (err) {
        yield put(actions.purchaseBurgerFail(err));
    }
}
export function* fetchOrdersSaga(action) {
    yield put(actions.fetchOrderStart());
    const queryParams = '?auth=' + action.token + '&orderBy="userId"&equalTo="' + action.userId + '"';
    try {
        const response = yield axios.get('/orders.json' + queryParams);
        let fetchedOrders = Object.keys(response.data).map(hashKey => {
            return { ...response.data[hashKey], id: hashKey };
        });
        yield put(actions.fetchOrderSuccess(fetchedOrders));
    }
    catch (err) {
        yield put(actions.fetchOrderFail(err));
    }
}
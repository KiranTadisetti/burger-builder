import { put } from 'redux-saga/effects';

import axios from '../../axios-orders'

import * as actions from '../actions/';

export function* initIntegredientSaga(action) {
    try {
        const response = yield axios.get('/ingredients.json')
        yield put(actions.setIngredients({ ingredients: response.data }));

    }
    catch (err){
        yield put(actions.fetchIngredientFailed());
    }
}